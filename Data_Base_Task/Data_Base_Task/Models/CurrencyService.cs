﻿using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System.Threading;
using System.Threading.Tasks;
using System.Text;
using System.Xml.Linq;

namespace Data_Base_Task.Models
{
    public class CurrencyService : BackgroundService
    {
        private readonly IMemoryCache memoryCache;

        public CurrencyService(IMemoryCache memoryCache)
        {
            this.memoryCache = memoryCache;
        }
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                try
                {
                    Thread.CurrentThread.CurrentCulture = new CultureInfo("ru-RU");

                    Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

                    XDocument xml = XDocument.Load("http://www.cbr.ru/scripts/XML_daily.asp");

                    Currency currency = new Currency();
                    currency.NameCurrency = Convert.ToString(xml.Elements("ValCurs").Elements("Valute").Elements("CharCode"));
                    currency.RateCurrency = Convert.ToDecimal(xml.Elements("ValCurs").Elements("Valute").Elements("Value"));

                    memoryCache.Set("key_currency", currency, TimeSpan.FromMinutes(1440));
                }
                catch(Exception e)
                {
                    //
                }
                await Task.Delay(3600000, stoppingToken);
            }
        }
    }
}
