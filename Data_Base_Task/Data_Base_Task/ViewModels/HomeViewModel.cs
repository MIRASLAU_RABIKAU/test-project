﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Data_Base_Task.Models;
using Data_Base_Task.ViewModels.CurrenciesViewModel;

namespace Data_Base_Task.ViewModels
{
    public class HomeViewModel
    {
        public IEnumerable<Currency> Currencies { get; set; }
        public PageViewModel PageViewModel { get; set; }
    }
}
