﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Data_Base_Task.Models;
using Microsoft.EntityFrameworkCore;
using Data_Base_Task.ViewModels;
using Data_Base_Task.ViewModels.CurrenciesViewModel;
using Microsoft.AspNetCore.Authorization;

namespace Data_Base_Task.Controllers
{
    //[Authorize(Roles = "User")]
    [Route("api/[controller]")]
    [ApiController]
    public class CurrenciesController : Controller
    {
        private TaskContext db;

        public CurrenciesController(TaskContext _db)
        {
            db = _db;
        }

        // GET: api/Currencies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Currency>>> Get(int page = 1)
        {
            int pageSize = 10;
            IQueryable<Currency> source = db.Currencies;
            var count = source.Count();
            var items = source.Skip((page - 1) * pageSize).Take(pageSize).ToList();
            PageViewModel pageViewModel = new PageViewModel(count, page, pageSize);
            CurrenciesViewModel currencies = new CurrenciesViewModel
            {
                Currencies = items,
                PageViewModel = pageViewModel
            };
            return View(currencies);
        }

        // GET api/currencies/id
        [HttpGet("{id}")]
        public async Task<ActionResult<Currency>> Details(int? id)
        {
           if(id == null)
           {
                return NotFound();
           }

            var currency = await db.Currencies
                 .FirstOrDefaultAsync(c => c.CurrencyID == id);
            if(currency == null)
            {
                return NotFound();
            }
            return View(currency);
        }
    }
}
